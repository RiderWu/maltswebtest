package com.web;

import java.awt.Rectangle;
//import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class Common {

	public static WebDriver driver = null;
	//public static AppiumDriver<WebElement> appdriver = null;
	public static String platform = null;
	public static String osName = System.getProperties().getProperty("os.name");
	//public static String osArch = System.getProperties().getProperty("os.arch");

	public static WebDriver openBrowser(String platformName, String browserName, String url, String width, String height) throws Exception {
		try {
			platform = platformName;
			if (platform.equalsIgnoreCase("Windows")&&browserName.equalsIgnoreCase("FireFox")) {
				DesiredCapabilities capability = DesiredCapabilities.firefox();
				capability.setCapability("marionette", false);
				if (osName.toLowerCase().contains("win")) {
					System.setProperty("webdriver.gecko.driver", "res/geckodriver.exe");
				} else if (osName.toLowerCase().contains("mac")){
					System.setProperty("webdriver.gecko.driver", "res/mac/geckodriver");
				}
				driver = new FirefoxDriver(capability);
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (platform.equalsIgnoreCase("Windows")&&browserName.equalsIgnoreCase("Chrome")) {
				if (osName.toLowerCase().contains("win")) {
					System.setProperty("webdriver.chrome.driver", "res/chromedriver.exe");
				} else if (osName.toLowerCase().contains("mac")){
					System.setProperty("webdriver.chrome.driver", "res/mac/chromedriver");
				}
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-infobars");
	            options.addArguments("--disable-popup-blocking");
	            options.addArguments("no-sandbox");
	            options.addArguments("disable-extensions");
	            options.addArguments("no-default-browser-check");
	            Map<String, Object> prefs = new HashMap<String, Object>();
	            prefs.put("credentials_enable_service", false);
	            prefs.put("profile.password_manager_enabled", false);
	            options.setExperimentalOption("prefs", prefs);
				driver = new ChromeDriver(options);
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (platform.equalsIgnoreCase("Windows")&&browserName.equalsIgnoreCase("IE")) {
				DesiredCapabilities capability = new DesiredCapabilities();
				capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				capability.setCapability("ignoreProtectedModeSettings", true);
				System.setProperty("webdriver.ie.driver", "res/IEDriverServer.exe");
				driver = new InternetExplorerDriver(capability);
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (platform.equalsIgnoreCase("Mac")&&browserName.equalsIgnoreCase("Safari")) {
				System.setProperty("webdriver.chrome.driver", "res/mac/WebKit-SVN-r216352.dmg");
				driver = new SafariDriver();
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (platform.equalsIgnoreCase("Android")&&browserName.equalsIgnoreCase("Chrome")) {
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
				capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.4.2");
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "LP Galaxy S4");
				capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
				driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				//driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (platform.equalsIgnoreCase("IOS")&&browserName.equalsIgnoreCase("Safari")) {
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
				capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.0");
				capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6");
				capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Safari");
				driver = new IOSDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				//driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			}

		} catch (TimeoutException ignored) {
			System.out.println("Browser load timeout.");
			((JavascriptExecutor) driver).executeScript("window.stop()");
			return driver;
		} catch (Exception e) {
			System.out.println("Browser setting error.");
			e.printStackTrace();
			driver.quit();
			return null;
		}

		return driver;
	}

	public static void wait(int timeout, String xpath) {
		(new WebDriverWait(driver, timeout)).until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver d) {
				return d.findElement(By.xpath(xpath));
			}
		});
	}

	@SuppressWarnings("unchecked")
	public static void scrollTo(int height) {
		if(platform.equalsIgnoreCase("Windows")||platform.equalsIgnoreCase("Mac")){
			((JavascriptExecutor) driver).executeScript(String.format("window.scrollTo(0, %d)", height));
		} else {
			((AppiumDriver<WebElement>)driver).executeScript(String.format("window.scrollTo(0, %d)", height));
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void scrollTo(WebElement element) {
		if(platform.equalsIgnoreCase("Windows")||platform.equalsIgnoreCase("Mac")){
			((JavascriptExecutor) driver).executeScript(String.format("window.scrollTo(0, %d)", element.getLocation().getY()));
		} else {
			((AppiumDriver<WebElement>)driver).executeScript(String.format("window.scrollTo(0, %d)", element.getLocation().getY()));
		}
	}
	
	public static void takeScreenShot(WebDriver driver, WebElement element, String foldername, String timeStamp, String testCaseName,
			String index) throws Exception {
		try {
			String regEx = "[|\\:;\"'<>,.?/]";
			index = Pattern.compile(regEx).matcher(index).replaceAll("").trim();
			/*
			if (platform.equalsIgnoreCase("Windows")) {
				String path = "/screenshots/" + timeStamp + "/" + foldername + "/" + testCaseName + "/" + index + ".png";
				File file = new File(path);
				
				//FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), file);
				
				if(!file.exists()) file.mkdirs();
				BufferedImage image = new Robot().createScreenCapture(new Rectangle(driver.manage().window().getPosition().getX(),driver.manage().window().getPosition().getY(),driver.manage().window().getSize().getWidth(),driver.manage().window().getSize().getHeight()));
				ImageIO.write(image, "png", file);
				
				if(null != element) {
					Rectangle area = new Rectangle(element.getLocation().getX(), element.getLocation().getY(), element.getSize().getWidth(), element.getSize().getHeight());
					cutImage(path,area);
				}
				
			} else 
			*/
			if (platform.equalsIgnoreCase("Mac")) {
				File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				String path = System.getProperty("user.dir") + "/screenshots/" + timeStamp + "/" + foldername + "/" + testCaseName + "/";
				File file = new File(path);
				if(!file.exists()) file.mkdirs();
				FileUtils.copyFile(scrFile, new File(path + index + ".png"));
			} else {//if (platform.equalsIgnoreCase("Android")) {
				String path = "screenshots/" + timeStamp + "/" + foldername + "/" + testCaseName + "/" + index + ".png";
				File file = new File(path);
				FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), file);
			}

		} catch (Exception e) {
			throw (e);
		}

	}
	
	/*
	public static void scrollDownAndTakeScreenShot(WebDriver driver, int browserheight, int headerheight, WebElement element, String foldername, String timeStamp, String testCaseClass,
			String name) throws Exception {
		try {
			int browserbarheight=0;
			if(platform.equalsIgnoreCase("Windows")||platform.equalsIgnoreCase("Mac")){
				browserbarheight=125;
			} else if(platform.equalsIgnoreCase("Android")){
				browserbarheight=80;
			}
			int barheight=browserbarheight+headerheight, h=0, count=1;
			
			Common.takeScreenShot(driver, null, foldername, timeStamp, testCaseClass, 
					name + count);
			count++;
			
			//Here I met a bug in Appium.  Appium don't support driver.manage().window().getSize().getHeight(). Then I transmit browserheight instead.
			while (h < driver.findElement(By.xpath("//body")).getSize().getHeight()-(browserheight-barheight)) {
				Common.scrollTo(h+(browserheight-barheight));
				//Thread.sleep(3000);
				Common.takeScreenShot(driver, null, foldername, timeStamp, testCaseClass, 
						name + count);
				h += (browserheight-barheight);
				count++;
			}

		} catch (Exception e) {
			throw (e);
		}
	}
	*/
	
	@SuppressWarnings("unchecked")
	public static void scrollDownAndTakeScreenShot(WebDriver driver, int browserheight, int headerheight, WebElement element, String foldername, String timeStamp, String testCaseClass,
			String name) throws Exception {
		try {
			int browserbarheight=0;
			if(platform.equalsIgnoreCase("Windows")||platform.equalsIgnoreCase("Mac")){
				browserbarheight=240;
			} else if(platform.equalsIgnoreCase("Android")){
				browserbarheight=80;
			}
			int barheight=browserbarheight+headerheight, h=0, count=1;
			
			Common.takeScreenShot(driver, null, foldername, timeStamp, testCaseClass, 
					name + count);
			count++;
			
			int scrollHeight=0;
			if(platform.equalsIgnoreCase("Windows")||platform.equalsIgnoreCase("Mac")){
				scrollHeight=Integer.parseInt(((JavascriptExecutor) driver).executeScript(String.format("return document.body.scrollHeight;")).toString());
			} else {
				scrollHeight=Integer.parseInt(((AppiumDriver<WebElement>)driver).executeScript(String.format("return document.body.scrollHeight;")).toString());
			}
			
			//Here I met a bug in Appium.  Appium don't support driver.manage().window().getSize().getHeight(). Then I transmit browserheight instead.
			while (h < scrollHeight-(browserheight-barheight)) {
				Common.scrollTo(h+(browserheight-barheight));
				//Thread.sleep(3000);
				Common.takeScreenShot(driver, null, foldername, timeStamp, testCaseClass, 
						name + count);
				h += (browserheight-barheight);
				count++;
			}

		} catch (Exception e) {
			throw (e);
		}
	}
	
	public static void cutImage(String path, Rectangle rectangle) throws Exception{
		try {
			int offsetX=5,offsetY=80,offsetW=10,offsetH=10;
			rectangle.setLocation((int)rectangle.getX()+offsetX, (int)rectangle.getY()+offsetY);
			rectangle.setSize((int)rectangle.getWidth()+offsetW, (int)rectangle.getHeight()+offsetH);
	        ImageReader reader = (ImageReader)ImageIO.getImageReadersByFormatName("PNG").next();  
	        InputStream inputStream = new FileInputStream(path);    
	        ImageInputStream iis = ImageIO.createImageInputStream(inputStream);     
	        reader.setInput(iis, true);     
	        ImageReadParam param = reader.getDefaultReadParam();        
	        param.setSourceRegion(rectangle);     
	        BufferedImage bi = reader.read(0,param);   
	        ImageIO.write(bi, "PNG", new File(path));  
	    } catch (Exception e) {
			throw (e);
		}
    }
	
	@SuppressWarnings("unchecked")
	public static void switchToNewTab(WebDriver driver){
		if(platform.equalsIgnoreCase("Windows")||platform.equalsIgnoreCase("Mac")){
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
		} else {
			((AppiumDriver<WebElement>)driver).context(((AppiumDriver<WebElement>)driver).getContextHandles().toArray()[0].toString());
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void switchBackToInitialTab(WebDriver driver){
		if(platform.equalsIgnoreCase("Windows")||platform.equalsIgnoreCase("Mac")){
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
		} else {
			((AppiumDriver<WebElement>)driver).context(((AppiumDriver<WebElement>)driver).getContextHandles().toArray()[((AppiumDriver<WebElement>)driver).getContextHandles().size()-1].toString());
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void pressKey(WebDriver driver, int keycode){
		if(platform.equalsIgnoreCase("Android")){
			((AndroidDriver<WebElement>)driver).pressKeyCode(keycode);
		}
	}

}
