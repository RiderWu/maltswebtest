package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;

import io.appium.java_client.android.AndroidKeyCode;

import com.pageobject.FriendsOfTheClassicMaltsPage;
import com.pageobject.Navigation;

public class NavigationTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "home/?agp=true";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Android") String _platform, @Optional("Chrome") String _browser, @Optional("en-gb") String _market, @Optional("1300") String _width, @Optional("800") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	@Test
	public void Test_NavigationLogo() throws Exception {
		try {
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Logo");
			
			navigation.getLogo().click();
			Thread.sleep(1000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLogo");
			
			driver.navigate().back();
			Thread.sleep(1000);
			
			Assert.assertTrue(navigation.getLogo().isEnabled(), "ExploreTheCollection is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_NavigationMenu_Desktop() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Menu");
			
			for(int i=0; i<6;i++){
				String name = navigation.getMenuDestop().get(i).getText();
				navigation.getMenuDestop().get(i).click();
				Thread.sleep(10000);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+name);
				driver.navigate().back();
				Thread.sleep(1000);
			}
			
			Assert.assertTrue(navigation.getMenuDestop().size()>6, "Missing Menu items");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_NavigationMenu_Mobile() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Menu");
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMenuButton");
			
			for(int i=0; i<6;i++){
				String name = navigation.getMenuMobile().get(i).getText();
				navigation.getMenuMobile().get(i).click();
				Thread.sleep(10000);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03-Click "+name);
				driver.navigate().back();
				Thread.sleep(1000);
				navigation.getMenuButtonMobile().click();
				Thread.sleep(1000);
			}
			
			Assert.assertTrue(navigation.getMenuButtonMobile().isEnabled(), "MenuButton is existing");
			Assert.assertTrue(navigation.getMenuMobile().size()>6, "Missing Menu items");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Register_Desktop() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Register");
			
			navigation.getRegisterDesktop().click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay1");
			
			FriendsOfTheClassicMaltsPage fotcmpage = new FriendsOfTheClassicMaltsPage(driver);
			fotcmpage.getTitle().selectByValue("Mr.");
			fotcmpage.getFirstName().sendKeys("Rider");
			fotcmpage.getSurname().sendKeys("Wu");
			fotcmpage.getDay().selectByValue("1");
			fotcmpage.getMonth().selectByValue("1");
			fotcmpage.getYear().selectByValue("1980");
			fotcmpage.getCountry().selectByValue("GB|18|75|en");
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay2");
			
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementsByClassName('registration-form-modal')[0].scrollTop=10000"));
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay3");
			
			fotcmpage.getEmailAddress().sendKeys("Rider.Wu@mullenloweprofero.com");
			fotcmpage.getPassword().sendKeys("Profero@123");
			fotcmpage.getConfirmPassword().sendKeys("Profero@123");
			fotcmpage.getReceiveEmailsCheckBox().click();
			fotcmpage.getAgreeCheckBox().click();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay4");
			
			fotcmpage.getTermsAndConditions().click();
			Common.switchToNewTab(driver);
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickTermsAndConditions");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			fotcmpage.getPrivacyAndCookies().click();
			Common.switchToNewTab(driver);
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickPrivacyAndCookies");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementsByClassName('registration-form-modal')[0].scrollTop=0"));
			fotcmpage.getCloseButton().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickCloseButton");
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			Thread.sleep(5000);
			
			Assert.assertTrue(navigation.getRegisterDesktop().isEnabled(), "RegisterDesktop is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Register_Mobile() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Register");
			
			navigation.getMenuMobile().get(9).click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay1");
			
			FriendsOfTheClassicMaltsPage fotcmpage = new FriendsOfTheClassicMaltsPage(driver);
			fotcmpage.getTitle().selectByValue("Mr.");
			fotcmpage.getFirstName().sendKeys("Rider");
			fotcmpage.getSurname().sendKeys("Wu");
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay2");
			
			fotcmpage.getDay().selectByValue("1");
			fotcmpage.getMonth().selectByValue("1");
			fotcmpage.getYear().selectByValue("1980");
			fotcmpage.getCountry().selectByValue("GB|18|75|en");
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay3");
			
			fotcmpage.getEmailAddress().sendKeys("Rider.Wu@mullenloweprofero.com");
			fotcmpage.getPassword().sendKeys("Profero@123");
			fotcmpage.getConfirmPassword().sendKeys("Profero@123");
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay4");
			
			fotcmpage.getReceiveEmailsCheckBox().click();
			fotcmpage.getAgreeCheckBox().click();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-RegistrationOverlay5");
			
			fotcmpage.getTermsAndConditions().click();
			Common.switchToNewTab(driver);
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickTermsAndConditions");
			//Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
			//		"03-ClickTermsAndConditions");
			Common.pressKey(driver, AndroidKeyCode.BACK);
			Common.switchBackToInitialTab(driver);
			Thread.sleep(1000);
			
			fotcmpage.getPrivacyAndCookies().click();
			Common.switchToNewTab(driver);
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickPrivacyAndCookies");
			//Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
			//		"04-ClickPrivacyAndCookies");
			Common.pressKey(driver, AndroidKeyCode.BACK);
			Common.switchBackToInitialTab(driver);
			Thread.sleep(1000);
			
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=0"));
			fotcmpage.getCloseButton().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickCloseButton");
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			Thread.sleep(5000);
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(3000);
			
			Assert.assertTrue(navigation.getMenuMobile().get(9).isEnabled(), "RegisterMobile is enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Signin_Desktop() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-SignIn");
			
			navigation.getSigninDesktop().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickSignin");
			
			FriendsOfTheClassicMaltsPage fotcmpage = new FriendsOfTheClassicMaltsPage(driver);
			Common.scrollTo(fotcmpage.getLoginForm());
			fotcmpage.getLoginEmail().sendKeys("Rider.Wu@mullenloweprofero.com");
			fotcmpage.getLoginPassword().sendKeys("Profero@123");
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputEmailAndPassword");
			
			fotcmpage.getLoginButton().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickLoginButton");
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Account");
			navigation.getAccountDesktop().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickAccount");
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-Logout");
			navigation.getLogoutDesktop().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickLogout");
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			Thread.sleep(10000);
			
			Assert.assertTrue(navigation.getSigninDesktop().isEnabled(), "SigninDesktop is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Signin_Mobile() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Signin");
			
			navigation.getMenuMobile().get(8).click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLogin");
			
			FriendsOfTheClassicMaltsPage fotcmpage = new FriendsOfTheClassicMaltsPage(driver);
			Common.scrollTo(fotcmpage.getLoginForm());
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputEmailAndPassword1");
			fotcmpage.getLoginEmail().sendKeys("Rider.Wu@mullenloweprofero.com");
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputEmailAndPassword2");
			fotcmpage.getLoginPassword().sendKeys("Profero@123");
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputEmailAndPassword3");
			
			fotcmpage.getLoginButton().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickLoginButton");
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-Account");
			navigation.getMenuMobile().get(8).click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickAccount");
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-Logout");
			navigation.getMenuMobile().get(9).click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickLogout");
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			Thread.sleep(10000);
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			
			Assert.assertTrue(navigation.getMenuMobile().get(8).isEnabled(), "SigninMobile is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_WhereToBuy_Desktop() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-WhereToBuy");
			
			navigation.getWhereToBuyDesktop().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickWhereToBuy");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(navigation.getWhereToBuyDesktop().isEnabled(), "WhereToBuyDesktop is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_WhereToBuy_Mobile() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-WhereToBuy");
			
			navigation.getMenuMobile().get(6).click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickWhereToBuy");
			
			driver.navigate().back();
			Thread.sleep(10000);
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			
			Assert.assertTrue(navigation.getMenuMobile().get(6).isEnabled(), "WhereToBuyMobile is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Lagavulin360Videos_Mobile() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin360Videos");
			
			navigation.getMenuMobile().get(7).click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLagavulin360Videos");
			
			driver.navigate().back();
			Thread.sleep(10000);
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			
			Assert.assertTrue(navigation.getMenuMobile().get(7).isEnabled(), "Lagavulin360VideosMobile is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Locales_Desktop() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Locales");
			
			navigation.getLocalesDesktop().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLocalesMenu");
			
			for(int i=0;i<navigation.getDropDownLocalesDesktop().size();i++){
				String screenshotname = navigation.getDropDownLocalesDesktop().get(i).getText();
				navigation.getDropDownLocalesDesktop().get(i).click();
				Thread.sleep(10000);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03-Click "+screenshotname);
				
				Common.scrollTo(0);
				Thread.sleep(1000);
				navigation.getLocalesDesktop().click();
				Thread.sleep(3000);
			}
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			Thread.sleep(10000);
			navigation.getLocalesDesktop().click();
			Thread.sleep(3000);
			
			Assert.assertTrue(navigation.getLocalesDesktop().isEnabled(), "LocalesDesktop is not enabled");
			Assert.assertTrue(navigation.getDropDownLocalesDesktop().size()==7, "Locales amount is not 7");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Locales_Mobile() throws Exception {
		try {
			
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true"));
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Locales");
			
			navigation.getLocalesMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=1000"));
			Thread.sleep(1000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLocales");
			
			for(int i=0;i<navigation.getDropDownlocalesMobile().size();i++){
				String screenshotname = navigation.getDropDownlocalesMobile().get(i).getText();
				navigation.getDropDownlocalesMobile().get(i).click();
				Thread.sleep(10000);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03-Click "+screenshotname);
				
				navigation.getMenuButtonMobile().click();
				Thread.sleep(1000);
				((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
				Thread.sleep(1000);
				navigation.getLocalesMobile().click();
				Thread.sleep(1000);
				((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=1000"));
				Thread.sleep(1000);
			}
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			Thread.sleep(10000);
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			navigation.getLocalesMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=1000"));
			Thread.sleep(1000);
			
			Assert.assertTrue(navigation.getLocalesMobile().isEnabled(), "LocalesMobile is not enabled");
			Assert.assertTrue(navigation.getDropDownlocalesMobile().size()==7, "DropDownlocalesMobile amount is not 7");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
