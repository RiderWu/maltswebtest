package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
import com.dataprovider.TestDataProvider;

public class BrandsTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("en-gb") String _market, @Optional("1300") String _width, @Optional("800") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TaliskerPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TaliskerPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CardhuPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CardhuPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_LagavulinPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-LagavulinPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MortlachPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MortlachPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlenOrdPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlenOrdPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlendullanPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlendullanPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfDufftownPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfDufftownPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_AuchroiskPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-AuchroiskPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BenrinnesPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BenrinnesPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BlairAtholPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BlairAtholPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CaolIlaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CaolIlaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ClynelishPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClynelishPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_DailuainePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-DailuainePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CragganmorePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CragganmorePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_DalwhinniePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-DalwhinniePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_GlenElginPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GlenElginPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_GlenSpeyPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GlenSpeyPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_GlenkinchiePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GlenkinchiePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_GlenlossiePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GlenlossiePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_KnockandoPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-KnockandoPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_InchgowerPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-InchgowerPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_LinkwoodPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-LinkwoodPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MannochmorePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MannochmorePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ObanPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ObanPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_RoyalLochnagarPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-RoyalLochnagarPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_StrathmillPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-StrathmillPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TeaninichPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TeaninichPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
