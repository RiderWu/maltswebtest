package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.RegisterPage;
import com.web.Common;

public class RegisterPageTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "appdistillerychosen/languagechosen/%s/register/";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://staging.secure.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("english") String _market, @Optional("800") String _width, @Optional("1280") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_RegisterPageLoad() throws Exception {
		try {
			driver.navigate().to(String.format(domain + "/" + path, market));
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-RegisterPageLoad");
			
			Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_RegisterSuccess() throws Exception {
		try {
			driver.navigate().to(String.format(domain + "/" + path, market));
			Thread.sleep(10000);
			
			RegisterPage registerPage = new RegisterPage(driver);
			registerPage.getRegistrationTitle().selectByIndex(0);
			registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
			registerPage.getRegistrationLastName().sendKeys("LastNameTester");
			registerPage.getRegistrationDay().selectByIndex(1);
			registerPage.getRegistrationMonth().selectByIndex(1);
			registerPage.getRegistrationYear().selectByIndex(19);
			registerPage.getRegistrationCountry().selectByIndex(176);
			registerPage.getRegistrationEmail().sendKeys(java.util.UUID.randomUUID().toString()+"@gmail.com");
			registerPage.getRegistrationPassword().sendKeys("88888888");
			registerPage.getRegistrationPasswordConfirm().sendKeys("88888888");
			Common.scrollTo(2000);
			registerPage.getNewsletterCheckBox().click();
			registerPage.getAgreementCheckBox().click();
			Common.scrollTo(0);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FillWithValidData");
			Common.scrollTo(2000);
			registerPage.getRegisterButton().click();
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickRegisterButton");
			Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path + "?Fname=FirstNameTester", market).replace("register", "finish")), "Page redirection is wrong."+driver.getCurrentUrl());
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_RegisterFromCanada() throws Exception {
		try {
			driver.navigate().to(String.format(domain + "/" + path, market));
			Thread.sleep(10000);
			
			RegisterPage registerPage = new RegisterPage(driver);
			registerPage.getRegistrationCountry().selectByIndex(31);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-SelectCanada");
			Common.scrollTo(2000);
			registerPage.getRegisterButton().click();
			Thread.sleep(1000);
			Common.scrollTo(0);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-Validation");
			
			Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
		
		@Test
		public void Test_RegisterFromUSA() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationCountry().selectByIndex(177);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-SelectUSA");
				Common.scrollTo(2000);
				registerPage.getRegisterButton().click();
				Thread.sleep(1000);
				Common.scrollTo(0);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Validation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterRequiredFieldValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				Common.scrollTo(2000);
				registerPage.getRegisterButton().click();
				Thread.sleep(1000);
				Common.scrollTo(0);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-RequiredFieldValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterInvalidEmailValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
				registerPage.getRegistrationLastName().sendKeys("LastNameTester");
				registerPage.getRegistrationDay().selectByIndex(1);
				registerPage.getRegistrationMonth().selectByIndex(1);
				registerPage.getRegistrationYear().selectByIndex(19);
				registerPage.getRegistrationCountry().selectByIndex(176);
				registerPage.getRegistrationEmail().sendKeys("InvalidEmailAddress");
				registerPage.getRegistrationPassword().sendKeys("88888888");
				registerPage.getRegistrationPasswordConfirm().sendKeys("88888888");
				Common.scrollTo(2000);
				registerPage.getAgreementCheckBox().click();
				registerPage.getRegisterButton().click();
				Thread.sleep(1000);
				Common.scrollTo(0);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-InvalidEmailValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterPasswordDoNotMatchValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
				registerPage.getRegistrationLastName().sendKeys("LastNameTester");
				registerPage.getRegistrationDay().selectByIndex(1);
				registerPage.getRegistrationMonth().selectByIndex(1);
				registerPage.getRegistrationYear().selectByIndex(19);
				registerPage.getRegistrationCountry().selectByIndex(176);
				registerPage.getRegistrationEmail().sendKeys("ValidEmailAddress@gmail.com");
				registerPage.getRegistrationPassword().sendKeys("88888888");
				registerPage.getRegistrationPasswordConfirm().sendKeys("11111111");
				Common.scrollTo(2000);
				registerPage.getAgreementCheckBox().click();
				registerPage.getRegisterButton().click();
				Thread.sleep(1000);
				Common.scrollTo(0);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-PasswordDoNotMatchValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterInvalidPasswordValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
				registerPage.getRegistrationLastName().sendKeys("LastNameTester");
				registerPage.getRegistrationDay().selectByIndex(1);
				registerPage.getRegistrationMonth().selectByIndex(1);
				registerPage.getRegistrationYear().selectByIndex(19);
				registerPage.getRegistrationCountry().selectByIndex(176);
				registerPage.getRegistrationEmail().sendKeys("ValidEmailAddress@gmail.com");
				registerPage.getRegistrationPassword().sendKeys("666666");
				registerPage.getRegistrationPasswordConfirm().sendKeys("666666");
				Common.scrollTo(2000);
				registerPage.getAgreementCheckBox().click();
				registerPage.getRegisterButton().click();
				Thread.sleep(1000);
				Common.scrollTo(0);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-InvalidPasswordValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterInvalidDateValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
				registerPage.getRegistrationLastName().sendKeys("LastNameTester");
				registerPage.getRegistrationDay().selectByIndex(31);
				registerPage.getRegistrationMonth().selectByIndex(2);
				registerPage.getRegistrationYear().selectByIndex(19);
				registerPage.getRegistrationCountry().selectByIndex(176);
				registerPage.getRegistrationEmail().sendKeys("ValidEmailAddress@gmail.com");
				registerPage.getRegistrationPassword().sendKeys("88888888");
				registerPage.getRegistrationPasswordConfirm().sendKeys("88888888");
				Common.scrollTo(2000);
				registerPage.getAgreementCheckBox().click();
				registerPage.getRegisterButton().click();
				Thread.sleep(1000);
				Common.scrollTo(0);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-InvalidDateValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterExistedEmailValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
				registerPage.getRegistrationLastName().sendKeys("LastNameTester");
				registerPage.getRegistrationDay().selectByIndex(1);
				registerPage.getRegistrationMonth().selectByIndex(1);
				registerPage.getRegistrationYear().selectByIndex(19);
				registerPage.getRegistrationCountry().selectByIndex(176);
				registerPage.getRegistrationEmail().sendKeys("Rider.Wu@mullenloweprofero.com");
				registerPage.getRegistrationPassword().sendKeys("88888888");
				registerPage.getRegistrationPasswordConfirm().sendKeys("88888888");
				Common.scrollTo(2000);
				registerPage.getAgreementCheckBox().click();
				registerPage.getRegisterButton().click();
				Thread.sleep(3000);
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-ExistedEmailValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterDryCountryValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
				registerPage.getRegistrationLastName().sendKeys("LastNameTester");
				registerPage.getRegistrationDay().selectByIndex(1);
				registerPage.getRegistrationMonth().selectByIndex(1);
				registerPage.getRegistrationYear().selectByIndex(19);
				registerPage.getRegistrationCountry().selectByIndex(1);
				registerPage.getRegistrationEmail().sendKeys("ValidEmailAddress@gmail.com");
				registerPage.getRegistrationPassword().sendKeys("88888888");
				registerPage.getRegistrationPasswordConfirm().sendKeys("88888888");
				Common.scrollTo(2000);
				registerPage.getAgreementCheckBox().click();
				registerPage.getRegisterButton().click();
				Thread.sleep(3000);
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-DryCountryValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
		
		@Test
		public void Test_RegisterInvalidAgeValidation() throws Exception {
			try {
				driver.navigate().to(String.format(domain + "/" + path, market));
				Thread.sleep(10000);
				
				RegisterPage registerPage = new RegisterPage(driver);
				registerPage.getRegistrationFirstName().sendKeys("FirstNameTester");
				registerPage.getRegistrationLastName().sendKeys("LastNameTester");
				registerPage.getRegistrationDay().selectByIndex(1);
				registerPage.getRegistrationMonth().selectByIndex(1);
				registerPage.getRegistrationYear().selectByIndex(18);
				registerPage.getRegistrationCountry().selectByIndex(176);
				registerPage.getRegistrationEmail().sendKeys("ValidEmailAddress@gmail.com");
				registerPage.getRegistrationPassword().sendKeys("88888888");
				registerPage.getRegistrationPasswordConfirm().sendKeys("88888888");
				Common.scrollTo(2000);
				registerPage.getAgreementCheckBox().click();
				registerPage.getRegisterButton().click();
				Thread.sleep(1000);
				Common.scrollTo(0);
				Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-InvalidAgeValidation");
				
				Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());
				
			} catch (Exception e) {

				e.printStackTrace();
				Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"99-ERROR");
				Assert.assertTrue(false, e.getMessage());
				
			}
		}
	}
