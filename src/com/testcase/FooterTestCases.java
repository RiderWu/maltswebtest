package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;

import com.dataprovider.TestDataProvider;
import com.pageobject.Footer;

public class FooterTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "home/?agp=true";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("en-gb") String _market, @Optional("1300") String _width, @Optional("800") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
		
		driver.navigate().to(domain + "/" + market + "/" + path);
		driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	private void CompareLinkWithExpected(WebElement linkDesktop, WebElement linkMobile, String expected){
		String expectURL = null;
		if(expected.startsWith("/")){
			expectURL = domain+expected;
		} else {
			expectURL = expected;
		}
		if(platform.equals("Windows")||platform.equals("Mac")){
			Assert.assertTrue(linkDesktop.getAttribute("href").equals(expectURL), String.format("Address is wrong in %s", market));
		} else {
			Assert.assertTrue(linkMobile.getAttribute("href").equals(expectURL), String.format("Address is wrong in %s", market));
		}
	}
	
	@Test
	public void Test_Footer_Desktop() throws Exception {
		try {
			Footer footer = new Footer(driver);
			
			Common.scrollTo(footer.getMainFooter());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Footer");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Footer_Mobile() throws Exception {
		try {
			Footer footer = new Footer(driver);
			
			Common.scrollTo(footer.getMainFooter());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Footer");
			
			footer.getMoreMobile().click();
			Common.scrollTo(footer.getMoreMobile());
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickMore");
			
			footer.getBackToTopMobile().click();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickBackToTop");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterTop5ClassicsLink_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getTop5ClassicsDesktop().get(index),footer.getTop5ClassicsMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterTop5ClassicsLink_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getTop5ClassicsDesktop().get(index),footer.getTop5ClassicsMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterTop5ClassicsLink_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getTop5ClassicsDesktop().get(index),footer.getTop5ClassicsMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterTop5ClassicsLink_4(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getTop5ClassicsDesktop().get(index),footer.getTop5ClassicsMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterTop5ClassicsLink_5(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getTop5ClassicsDesktop().get(index),footer.getTop5ClassicsMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_4(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_5(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_6(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_7(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_8(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterAtoDLink_9(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getAtoDDesktop().get(index),footer.getAtoDMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterEtoHLink_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getEtoHDesktop().get(index),footer.getEtoHMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterEtoHLink_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getEtoHDesktop().get(index),footer.getEtoHMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterEtoHLink_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getEtoHDesktop().get(index),footer.getEtoHMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterEtoHLink_4(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getEtoHDesktop().get(index),footer.getEtoHMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterItoNLink_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getItoNDesktop().get(index),footer.getItoNMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterItoNLink_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getItoNDesktop().get(index),footer.getItoNMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterItoNLink_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getItoNDesktop().get(index),footer.getItoNMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterItoNLink_4(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getItoNDesktop().get(index),footer.getItoNMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterItoNLink_5(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getItoNDesktop().get(index),footer.getItoNMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterItoNLink_6(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getItoNDesktop().get(index),footer.getItoNMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_4(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_5(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_6(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_7(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_8(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterOtoZLink_9(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getOtoZDesktop().get(index),footer.getOtoZMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterGuideToWhiskyLink_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getGuideToWhiskyDesktop().get(index),footer.getGuideToWhiskyMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterGuideToWhiskyLink_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getGuideToWhiskyDesktop().get(index),footer.getGuideToWhiskyMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterGuideToWhiskyLink_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getGuideToWhiskyDesktop().get(index),footer.getGuideToWhiskyMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterGuideToWhiskyLink_4(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getGuideToWhiskyDesktop().get(index),footer.getGuideToWhiskyMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterGuideToWhiskyLink_5(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getGuideToWhiskyDesktop().get(index),footer.getGuideToWhiskyMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterGuideToWhiskyLink_6(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getGuideToWhiskyDesktop().get(index),footer.getGuideToWhiskyMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterFriendsOfTheClassicMaltsLink_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getFriendsOfTheClassicMaltsDesktop().get(index),footer.getFriendsOfTheClassicMaltsMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterFollowUsLinks_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getFollowUsLinksDesktop().get(index),footer.getFollowUsLinksMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterFollowUsLinks_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getFollowUsLinksDesktop().get(index),footer.getFollowUsLinksMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterFollowUsLinks_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getFollowUsLinksDesktop().get(index),footer.getFollowUsLinksMobile().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterBottomLinks_1(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getBottomLinks().get(index),footer.getBottomLinks().get(index),param.get(market).toString().replace("www.malts.com", domain.split("//")[1]));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterBottomLinks_2(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getBottomLinks().get(index),footer.getBottomLinks().get(index),param.get(market).toString().replace("www.malts.com", domain.split("//")[1]));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterBottomLinks_3(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getBottomLinks().get(index),footer.getBottomLinks().get(index),param.get(market).toString().replace("www.malts.com", domain.split("//")[1]));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterBottomLinks_4(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getBottomLinks().get(index),footer.getBottomLinks().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterBottomLinks_5(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getBottomLinks().get(index),footer.getBottomLinks().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterBottomLinks_6(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getBottomLinks().get(index),footer.getBottomLinks().get(index),param.get(market).toString());
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_FooterBottomLinks_7(Map<?, ?> param) throws Exception {
		Footer footer = new Footer(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		CompareLinkWithExpected(footer.getBottomLinks().get(index),footer.getBottomLinks().get(index),param.get(market).toString());
	}
}
