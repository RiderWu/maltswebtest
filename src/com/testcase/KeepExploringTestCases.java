package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
import com.pageobject.KeepExploring;

public class KeepExploringTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "home/?agp=true";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("en-gb") String _market, @Optional("1300") String _width, @Optional("800") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_KeepExploringWithoutSignin() throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().deleteAllCookies();
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			KeepExploring keepexploring = new KeepExploring(driver);
			Common.scrollTo(keepexploring.getKeepExploring());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-KeepExploring");
			
			keepexploring.getDiscoverOurCollection().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickDiscoverOurCollection");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			keepexploring.getDiscoverNewWhiskies().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickDiscoverNewWhiskies");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			keepexploring.getDiscoverSingleMalts().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickDiscoverSingleMalts");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			keepexploring.getRegister().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickRegister");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(keepexploring.getDiscoverOurCollection().isEnabled(), "DiscoverOurCollection is not enabled");
			Assert.assertTrue(keepexploring.getDiscoverNewWhiskies().isEnabled(), "DiscoverNewWhiskies is not enabled");
			Assert.assertTrue(keepexploring.getDiscoverSingleMalts().isEnabled(), "DiscoverSingleMalts is not enabled");
			Assert.assertTrue(keepexploring.getRegister().isEnabled(), "Register is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_KeepExploringWithSignin() throws Exception {
		try {
			driver.navigate().to(domain + "/" + market + "/" + path);
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			ShareSteps.Signin(driver, platform);
			driver.navigate().to(domain + "/" + market + "/" + path);
			Thread.sleep(10000);
			
			KeepExploring keepexploring = new KeepExploring(driver);
			Common.scrollTo(keepexploring.getKeepExploring());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-KeepExploring");
			
			Assert.assertTrue(keepexploring.getDiscoverOurCollection().isEnabled(), "DiscoverOurCollection is not enabled");
			Assert.assertTrue(keepexploring.getDiscoverNewWhiskies().isEnabled(), "DiscoverNewWhiskies is not enabled");
			Assert.assertTrue(keepexploring.getDiscoverSingleMalts().isEnabled(), "DiscoverSingleMalts is not enabled");
			Assert.assertTrue(keepexploring.getRegister()==null, "Register should be disabled after Sign in");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
