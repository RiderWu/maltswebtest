package com.testcase;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.web.Common;

import com.pageobject.FriendsOfTheClassicMaltsPage;
import com.pageobject.Navigation;

public class ShareSteps {
	
	public static void Signin(WebDriver driver, String platform) throws Exception {
		if(platform.equals("Windows")||platform.equals("Mac")){
			
			Navigation navigation = new Navigation(driver);
			
			navigation.getSigninDesktop().click();
			Thread.sleep(10000);
			
			FriendsOfTheClassicMaltsPage fotcmpage = new FriendsOfTheClassicMaltsPage(driver);
			Common.scrollTo(fotcmpage.getLoginForm());
			fotcmpage.getLoginEmail().sendKeys("Rider.Wu@mullenloweprofero.com");
			fotcmpage.getLoginPassword().sendKeys("Profero@123");
			
			fotcmpage.getLoginButton().click();
			Thread.sleep(10000);
		} else {
			
			Navigation navigation = new Navigation(driver);
			
			navigation.getMenuButtonMobile().click();
			Thread.sleep(1000);
			((JavascriptExecutor) driver).executeScript(String.format("document.getElementById('mCSB_1').scrollTop=100"));
			Thread.sleep(1000);
			
			navigation.getMenuMobile().get(8).click();
			Thread.sleep(10000);
			
			FriendsOfTheClassicMaltsPage fotcmpage = new FriendsOfTheClassicMaltsPage(driver);
			Common.scrollTo(fotcmpage.getLoginForm());
			Thread.sleep(3000);
			
			fotcmpage.getLoginEmail().sendKeys("Rider.Wu@mullenloweprofero.com");
			fotcmpage.getLoginPassword().sendKeys("Profero@123");
			
			fotcmpage.getLoginButton().click();
			Thread.sleep(10000);
		}	
	}
	
}
