package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.BeginPage;
import com.web.Common;

public class BeginPageTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "appdistillerychosen/languagechosen/%s/begin/";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://staging.secure.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("english") String _market, @Optional("800") String _width, @Optional("1280") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_BeginPageLoad() throws Exception {
		try {
			driver.navigate().to(String.format(domain + "/" + path, market));
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BeginPageLoad");
			
			Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market)), "Page redirection is wrong."+driver.getCurrentUrl());

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_ClickJoinTheFriendsOfTheClassicMalts() throws Exception {
		try {
			driver.navigate().to(String.format(domain + "/" + path, market));
			Thread.sleep(10000);
			BeginPage beginPage = new BeginPage(driver);
			beginPage.getJoinTHeFriendsOfTheClassicMalts().click();
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 0, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickJoinTheFriendsOfTheClassicMalts");
			Assert.assertTrue(driver.getCurrentUrl().equals(String.format(domain + "/" + path, market).replace("begin", "register")), "Page redirection is wrong."+driver.getCurrentUrl());
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
