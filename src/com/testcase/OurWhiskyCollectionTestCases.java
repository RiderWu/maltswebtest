package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
import com.dataprovider.TestDataProvider;
import com.pageobject.OurWhiskyCollectionPage;

public class OurWhiskyCollectionTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "our-whisky-collection/?agp=true";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("en-gb") String _market, @Optional("1300") String _width, @Optional("800") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
		
		if(market.equals("fr-fr")){
			path = "collection-de-whiskies/?agp=true";
		}
		
		driver.navigate().to(domain + "/" + market + "/" + path);
		driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
		Thread.sleep(10000);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_OurWhiskyCollectionPageLoad() throws Exception {
		try {
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-OurWhiskyCollectionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageCore5Brand1(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		Assert.assertTrue(ourwhiskycollectionpage.getCore5Brand1().findElement(By.tagName("a")).getAttribute("href").equals(domain+param.get(market).toString()), String.format("Core5Brand1 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageCore5Brand2(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		Assert.assertTrue(ourwhiskycollectionpage.getCore5Brand2().findElement(By.tagName("a")).getAttribute("href").equals(domain+param.get(market).toString()), String.format("Core5Brand2 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageCore5Brand3(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		Assert.assertTrue(ourwhiskycollectionpage.getCore5Brand3().findElement(By.tagName("a")).getAttribute("href").equals(domain+param.get(market).toString()), String.format("Core5Brand3 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageCore5Brand4(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		Assert.assertTrue(ourwhiskycollectionpage.getCore5Brand4().findElement(By.tagName("a")).getAttribute("href").equals(domain+param.get(market).toString()), String.format("Core5Brand4 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageCore5Brand5(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		Assert.assertTrue(ourwhiskycollectionpage.getCore5Brand5().findElement(By.tagName("a")).getAttribute("href").equals(domain+param.get(market).toString()), String.format("Core5Brand5 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageFindAWhisky(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		Assert.assertTrue(ourwhiskycollectionpage.getFindAWhisky().findElement(By.tagName("a")).getAttribute("href").equals(domain+param.get(market).toString()), String.format("FindAWhisky address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_1(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection1 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_2(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection2 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_3(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection3 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_4(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection4 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_5(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection5 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_6(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection6 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_7(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection7 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_8(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection8 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_9(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection9 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_10(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection10 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_11(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection11 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_12(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection12 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_13(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection13 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_14(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection14 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_15(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection15 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_16(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection16 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_17(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection17 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_18(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection18 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_19(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection19 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageOurCollection_20(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		int index = Integer.parseInt(Thread.currentThread().getStackTrace()[1].getMethodName().split("_")[Thread.currentThread().getStackTrace()[1].getMethodName().split("_").length-1])-1;
		Assert.assertTrue(ourwhiskycollectionpage.getOurCollectionLinks().get(index).getAttribute("href").equals(domain+param.get(market).toString()), String.format("OurCollection20 address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OurWhiskyCollectionPageViewOurSpecialReleases(Map<?, ?> param) throws Exception {
		OurWhiskyCollectionPage ourwhiskycollectionpage = new OurWhiskyCollectionPage(driver);
		Assert.assertTrue(ourwhiskycollectionpage.getViewOurSpecialReleases().getAttribute("href").equals(domain+param.get(market).toString()), String.format("ViewOurSpecialReleases address is wrong in %s", market));
	}
}
