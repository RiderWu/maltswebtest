package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
import com.dataprovider.TestDataProvider;
import com.pageobject.HomePage;

public class HomeTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "home/?agp=true";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("en-gb") String _market, @Optional("1300") String _width, @Optional("800") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
		
		driver.navigate().to(domain + "/" + market + "/" + path);
		driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
		Thread.sleep(10000);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_HomePageLoad() throws Exception {
		try {
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HomePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageExploreTheCollection(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getExploreTheCollection().getAttribute("href").equals(domain+param.get(market).toString()), String.format("ExploreTheCollection address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageFindYourWhisky(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getFindYourWhisky().getAttribute("href").equals(domain+param.get(market).toString()), String.format("FindYourWhisky address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageLearnMore(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getLearnMore().getAttribute("href").equals(domain+param.get(market).toString()), String.format("LearnMore address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageExploreNow(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getExploreNow().getAttribute("href").equals(domain+param.get(market).toString()), String.format("ExploreNow address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageSeeMore(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getSeeMore().getAttribute("href").equals(domain+param.get(market).toString()), String.format("SeeMore address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageExploreRegions(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getExploreRegions().getAttribute("href").equals(domain+param.get(market).toString()), String.format("ExploreRegions address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageExploreFlavourMap(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getExploreFlavourMap().getAttribute("href").equals(domain+param.get(market).toString()), String.format("ExploreFlavourMap address is wrong in %s", market));
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageExploreAtoZ(Map<?, ?> param) throws Exception {
		HomePage homepage = new HomePage(driver);
		
		Assert.assertTrue(homepage.getExploreAtoZ().getAttribute("href").equals(domain+param.get(market).toString()), String.format("ExploreAtoZ address is wrong in %s", market));
	}
}
