package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
import com.dataprovider.TestDataProvider;

public class ProductsTestCases {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String platform = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.malts.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "platform", "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Windows") String _platform, @Optional("Chrome") String _browser, @Optional("en-gb") String _market, @Optional("1300") String _width, @Optional("800") String _height) throws Exception {
		platform = _platform;
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(platform, browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TaliskerSkyePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TaliskerSkyePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TaliskerStormPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TaliskerStormPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TaliskerDarkStormPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TaliskerDarkStormPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TaliskerPortRuighePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TaliskerPortRuighePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Talisker10YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Talisker10YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TaliskerDistillersEditionPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TaliskerDistillersEditionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Talisker57NorthPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Talisker57NorthPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Talisker18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Talisker18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Talisker25YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Talisker25YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Talisker30YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Talisker30YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TaliskerNeistPointPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TaliskerNeistPointPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CardhuGoldReservePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CardhuGoldReservePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CardhuAmberRockPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CardhuAmberRockPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Cardhu12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Cardhu12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Cardhu15YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Cardhu15YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Cardhu18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Cardhu18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin16YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin16YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_LagavulinDistillersEditionPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-LagavulinDistillersEditionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin12YearsOld2015PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin12YearsOld2015PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin12YearsOld2014PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin12YearsOld2014PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin8YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin8YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin25YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin25YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin12YearsOld2002PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin12YearsOld2002PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin12YearsOld2003PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin12YearsOld2003PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin12YearsOld2004PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin12YearsOld2004PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin12YearsOld2005PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin12YearsOld2005PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin12YearOld200thAnniversaryPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin12YearOld200thAnniversaryPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin1991PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin1991PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin200PageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin200PageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Lagavulin360VideosPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Lagavulin360VideosPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_LagavulinSoundscapesPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-LagavulinSoundscapesPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MortlachSpecialStrengthPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MortlachSpecialStrengthPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Mortlach18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Mortlach18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Mortlach25YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Mortlach25YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlenOrd12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlenOrd12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlenOrd15YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlenOrd15YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlenOrd18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlenOrd18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlendullanClassicPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlendullanClassicPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlendullanDoubleMaturedPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlendullanDoubleMaturedPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlendullanMastersArtPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlendullanMastersArtPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlendullan12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlendullan12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlendullan15YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlendullan15YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfGlendullan18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfGlendullan18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfDufftownSpeyCascadePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfDufftownSpeyCascadePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfDufftownTailfirePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfDufftownTailfirePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfDufftownSunrayPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfDufftownSunrayPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfDufftown12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfDufftown12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfDufftown15YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfDufftown15YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_TheSingletonOfDufftown18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-TheSingletonOfDufftown18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Auchroisk10YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Auchroisk10YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Benrinnes15YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Benrinnes15YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BlairAthol12YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BlairAthol12YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CaolIlaMochPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CaolIlaMochPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CaolIla12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CaolIla12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CaolIlaDistillersEditionPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CaolIlaDistillersEditionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CaolIla18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CaolIla18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CaolIlaCaskStrengthPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CaolIlaCaskStrengthPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CaolIla25YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CaolIla25YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Clynelish14YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Clynelish14YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Cragganmore12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Cragganmore12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_CragganmoreDistillersEditionPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-CragganmoreDistillersEditionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Dailuaine16YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Dailuaine16YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_DalwhinnieWintersGoldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-DalwhinnieWintersGoldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Dalwhinnie15YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Dalwhinnie15YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_DalwhinnieDistillersEditionPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-DalwhinnieDistillersEditionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_GlenElgin12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GlenElgin12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_GlenSpey12YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GlenSpey12YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Glenkinchie12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Glenkinchie12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_GlenkinchieDistillersEditionPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-GlenkinchieDistillersEditionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Glenlossie10YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Glenlossie10YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Inchgower14YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Inchgower14YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Knockando12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Knockando12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Knockando15YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Knockando15YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Knockando18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Knockando18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Knockando21YearsOldMasterReservePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Knockando21YearsOldMasterReservePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Linkwood12YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Linkwood12YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Mannochmore12YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Mannochmore12YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ObanLittleBayPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ObanLittleBayPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Oban14YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Oban14YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ObanDistillersEditionPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ObanDistillersEditionPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Oban18YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Oban18YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_RoyalLochnagar12YearsOldPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-RoyalLochnagar12YearsOldPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_RoyalLochnagarSelectedReservePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-RoyalLochnagarSelectedReservePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Strathmill12YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Strathmill12YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_Teaninich10YearsOldFloraFaunaPageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(domain + "/" + market + "/" + param.get(market).toString());
			driver.manage().addCookie(new Cookie("malts-subscribe-popup","true",domain.replaceAll("[a-zA-z]+://", ""),"/",null,false));
			Thread.sleep(10000);
			
			Common.scrollDownAndTakeScreenShot(driver, Integer.parseInt(height), 100, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Teaninich10YearsOldFloraFaunaPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 600000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, platform+"_"+browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
