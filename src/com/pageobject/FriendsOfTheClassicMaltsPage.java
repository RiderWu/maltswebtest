package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class FriendsOfTheClassicMaltsPage {
public WebDriver driver = null;
	
	public FriendsOfTheClassicMaltsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "registrationTitle")
	private WebElement title;
	public Select getTitle(){
		Select selector = new Select(title);
		return selector;
	}
	
	@FindBy(id = "registrationFirstName")
	private WebElement firstName;
	public WebElement getFirstName(){
		return firstName;
	}
	
	@FindBy(id = "registrationLastName")
	private WebElement surname;
	public WebElement getSurname(){
		return surname;
	}
	
	@FindBy(id = "registrationDay")
	private WebElement day;
	public Select getDay(){
		Select selector = new Select(day);
		return selector;
	}
	
	@FindBy(id = "registrationMonth")
	private WebElement month;
	public Select getMonth(){
		Select selector = new Select(month);
		return selector;
	}
	
	@FindBy(id = "registrationYear")
	private WebElement year;
	public Select getYear(){
		Select selector = new Select(year);
		return selector;
	}
	
	@FindBy(id = "registrationCountry")
	private WebElement country;
	public Select getCountry(){
		Select selector = new Select(country);
		return selector;
	}
	
	@FindBy(id = "registrationEmail")
	private WebElement emailAddress;
	public WebElement getEmailAddress(){
		return emailAddress;
	}
	
	@FindBy(id = "registrationPassword")
	private WebElement password;
	public WebElement getPassword(){
		return password;
	}
	
	@FindBy(id = "registrationPasswordConfirm")
	private WebElement confirmPassword;
	public WebElement getConfirmPassword(){
		return confirmPassword;
	}
	
	@FindBy(css = "div.form-checkbox-fake-checkbox")
	private WebElement receiveEmailsCheckBox;
	public WebElement getReceiveEmailsCheckBox(){
		return receiveEmailsCheckBox;
	}
	
	@FindBy(css = "div.registration-form-agreement.form-group > label.form-checkbox > div.form-checkbox-fake-checkbox")
	private WebElement agreeCheckBox;
	public WebElement getAgreeCheckBox(){
		return agreeCheckBox;
	}
	
	@FindBy(css = "a[title='Terms and Conditions']")
	private WebElement termsAndConditions;
	public WebElement getTermsAndConditions(){
		return termsAndConditions;
	}
	
	@FindBy(css = "a[title='Privacy and Cookies']")
	private WebElement privacyAndCookies;
	public WebElement getPrivacyAndCookies(){
		return privacyAndCookies;
	}
	
	@FindBy(id = "btnRegister")
	private WebElement registerButton;
	public WebElement getRegisterButton(){
		return registerButton;
	}
	
	@FindBy(className = "modal-close")
	private WebElement closeButton;
	public WebElement getCloseButton(){
		return closeButton;
	}
	
	@FindBy(className = "my-malts-header-content-item-signin")
	private WebElement loginForm;
	public WebElement getLoginForm(){
		return loginForm;
	}
	
	@FindBy(id = "loginEmail")
	private WebElement loginemail;
	public WebElement getLoginEmail(){
		return loginemail;
	}
	
	@FindBy(id = "loginPassword")
	private WebElement loginPassword;
	public WebElement getLoginPassword(){
		return loginPassword;
	}
	
	@FindBys({
		@FindBy(id = "loginForm"),
		@FindBy(tagName = "a")
	})
	private WebElement forgottenPassword;
	public WebElement getForgottenPassword(){
		return forgottenPassword;
	}
	
	@FindBy(className = "button--submit")
	private WebElement loginButton;
	public WebElement getLoginButton(){
		return loginButton;
	}
	
	@FindBy(id = "emailFocus")
	private WebElement emailFocus;
	public WebElement getEmailFocus(){
		return emailFocus;
	}
	
	@FindBys({
		@FindBy(id = "forgetPasswordForm"),
		@FindBy(tagName = "a")
	})
	private WebElement loginSwitchOnForgetPassword;
	public WebElement getLoginSwitchOnForgetPassword(){
		return loginSwitchOnForgetPassword;
	}
	
	@FindBy(id = "btnSendMail")
	private WebElement passwordResetButton;
	public WebElement getPasswordResetButton(){
		return passwordResetButton;
	}
	
	@FindBy(id = "txtChangePassword")
	private WebElement changePassword;
	public WebElement getChangePassword(){
		return changePassword;
	}
	
	@FindBy(id = "txtConfirmChangePassword")
	private WebElement confirmChangePassword;
	public WebElement getConfirmChangePassword(){
		return confirmChangePassword;
	}
	
	@FindBys({
		@FindBy(id = "changePasswordForm"),
		@FindBy(tagName = "a")
	})
	private WebElement LoginSwitchOnChangePassword;
	public WebElement getloginSwitchOnChangePassword(){
		return LoginSwitchOnChangePassword;
	}
	
	@FindBy(id = "btnChangePassord")
	private WebElement changePassordButton;
	public WebElement getChangePassordButton(){
		return changePassordButton;
	}
}
