package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
public WebDriver driver = null;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "div.header-content > a")
	private WebElement exploreTheCollection;
	public WebElement getExploreTheCollection(){
		return exploreTheCollection;
	}
	
	@FindBy(xpath = "//div[@id='homepage-header']/div[3]/div/div/footer/a")
	private WebElement findYourWhisky;
	public WebElement getFindYourWhisky(){
		return findYourWhisky;
	}
	
	@FindBy(xpath = "//div[@id='homepage-header']/div[3]/div[2]/div/footer/a")
	private WebElement learnMore;
	public WebElement getLearnMore(){
		return learnMore;
	}
	
	@FindBy(xpath = "//div[@id='homepage-header']/div[3]/div[3]/div/footer/a")
	private WebElement exploreNow;
	public WebElement getExploreNow(){
		return exploreNow;
	}
	
	@FindBy(xpath = "//section[@id='Core5Brand']/div/div[2]/div[6]/div/div/a")
	private WebElement seeMore;
	public WebElement getSeeMore(){
		return seeMore;
	}
	
	@FindBy(xpath = "//section[@id='TripleContentBlock']/div/div[2]/div/div/div/a")
	private WebElement exploreRegions;
	public WebElement getExploreRegions(){
		return exploreRegions;
	}
	
	@FindBy(xpath = "//section[@id='TripleContentBlock']/div/div[2]/div/div[2]/div/a")
	private WebElement exploreFlavourMap ;
	public WebElement getExploreFlavourMap(){
		return exploreFlavourMap;
	}
	
	@FindBy(xpath = "//section[@id='TripleContentBlock']/div/div[2]/div/div[3]/div/a")
	private WebElement exploreAtoZ ;
	public WebElement getExploreAtoZ(){
		return exploreAtoZ;
	}
}
