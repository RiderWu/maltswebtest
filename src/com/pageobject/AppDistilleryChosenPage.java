package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AppDistilleryChosenPage {
public WebDriver driver = null;
	
	public AppDistilleryChosenPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "div.trade-booking-dropdown-select")
	private WebElement chooseDistilleryDropDownList;
	public WebElement getChooseDistilleryDropDownList(){
		return chooseDistilleryDropDownList;
	}
	
	@FindBy(css = "div.trade-booking-dropdown-block > ul > li")
	private List<WebElement> chooseDistilleryMenuItems;
	public List<WebElement> getChooseDistilleryMenuItems(){
		return chooseDistilleryMenuItems;
	}
	
	@FindBy(tagName = "button")
	private WebElement chooseSelectedDistillery;
	public WebElement getChooseSelectedDistillery(){
		return chooseSelectedDistillery;
	}
}
