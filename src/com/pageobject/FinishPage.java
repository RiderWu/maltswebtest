package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinishPage {
public WebDriver driver = null;
	
	public FinishPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "button.finish > a")
	private WebElement finishButton;
	public WebElement getFinishButton(){
		return finishButton;
	}
}
