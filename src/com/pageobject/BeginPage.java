package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class BeginPage {
public WebDriver driver = null;
	
	public BeginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBys({
		@FindBy(className = "mobile-header"),
		@FindBy(tagName = "a")
	})
	private WebElement joinTHeFriendsOfTheClassicMalts;
	public WebElement getJoinTHeFriendsOfTheClassicMalts(){
		return joinTHeFriendsOfTheClassicMalts;
	}
}
