package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class Navigation {
public WebDriver driver = null;
	
	public Navigation(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "navigation-logo")
	private WebElement logo;
	public WebElement getLogo(){
		return logo;
	}
	
	@FindBys({
		@FindBy(className = "navigation-main-menu"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> menuDestop;
	public List<WebElement> getMenuDestop(){
		return menuDestop;
	}
	
	@FindBy(css = "div.navigation > button")
	private WebElement menuButtonMobile;
	public WebElement getMenuButtonMobile(){
		return menuButtonMobile;
	}
	
	@FindBys({
		@FindBy(className = "navigation-sidebar-menu"),
		@FindBy(xpath = "li/a")
	})
	private List<WebElement> menuMobile;
	public List<WebElement> getMenuMobile(){
		return menuMobile;
	}
	
	@FindBy(css = "div.navigation-small-menu > ul > li:nth-child(3) > a:nth-child(2)")
	private WebElement registerDesktop;
	public WebElement getRegisterDesktop(){
		return registerDesktop;
	}
	
	@FindBy(css = "ul.navigation-sidebar-menu > li:nth-child(10) > a")
	private WebElement registerMobile;
	public WebElement getRegisterMobile(){
		return registerMobile;
	}
	
	@FindBy(css = "div.navigation-small-menu > ul > li:nth-child(3) > a:nth-child(1)")
	private WebElement signinDesktop;
	public WebElement getSigninDesktop(){
		return signinDesktop;
	}
	
	@FindBy(css = "div.navigation-small-menu > ul > li:nth-child(3) > a:nth-child(1)")
	private WebElement accountDesktop;
	public WebElement getAccountDesktop(){
		return accountDesktop;
	}
	
	@FindBy(css = "div.navigation-small-menu > ul > li:nth-child(4) > a:nth-child(1)")
	private WebElement logoutDesktop;
	public WebElement getLogoutDesktop(){
		return logoutDesktop;
	}
	
	@FindBy(css = "div.navigation-small-menu > ul > li:nth-child(2) > a:nth-child(1)")
	private WebElement whereToBuyDesktop;
	public WebElement getWhereToBuyDesktop(){
		return whereToBuyDesktop;
	}
	
	@FindBy(css = "div.navigation-small-menu > ul > li:nth-child(1) > div:nth-child(1) > a:nth-child(1)")
	private WebElement localesDesktop;
	public WebElement getLocalesDesktop(){
		return localesDesktop;
	}
	
	@FindBys({
		@FindBy(className = "dropdown-menu"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> dropDownlocalesDesktop;
	public List<WebElement> getDropDownLocalesDesktop(){
		return dropDownlocalesDesktop;
	}
	
	@FindBy(id = "language-selector-selected-language")
	private WebElement localesMobile;
	public WebElement getLocalesMobile(){
		return localesMobile;
	}
	
	@FindBys({
		@FindBy(className = "navigation-sidebar-menu-language-options"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> dropDownlocalesMobile;
	public List<WebElement> getDropDownlocalesMobile(){
		return dropDownlocalesMobile;
	}
}
