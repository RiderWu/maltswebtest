package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class OurWhiskyCollectionPage {
public WebDriver driver = null;
	
	public OurWhiskyCollectionPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "c5b1")
	private WebElement core5Brand1;
	public WebElement getCore5Brand1(){
		return core5Brand1;
	}
	
	@FindBy(id = "c5b2")
	private WebElement core5Brand2;
	public WebElement getCore5Brand2(){
		return core5Brand2;
	}
	
	@FindBy(id = "c5b3")
	private WebElement core5Brand3;
	public WebElement getCore5Brand3(){
		return core5Brand3;
	}
	
	@FindBy(id = "c5b4")
	private WebElement core5Brand4;
	public WebElement getCore5Brand4(){
		return core5Brand4;
	}
	
	@FindBy(id = "c5b5")
	private WebElement core5Brand5;
	public WebElement getCore5Brand5(){
		return core5Brand5;
	}
	
	@FindBy(xpath = "//section[@id='Core5Brand']/div/div/div[6]/div")
	private WebElement findAWhisky;
	public WebElement getFindAWhisky(){
		return findAWhisky;
	}
	
	@FindBys({
		@FindBy(id = "Ourcollection"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> ourCollectionLinks;
	public List<WebElement> getOurCollectionLinks(){
		return ourCollectionLinks;
	}
	
	@FindBy(css="a.button.button--purple")
	private WebElement viewOurSpecialReleases;
	public WebElement getViewOurSpecialReleases(){
		return viewOurSpecialReleases;
	}
}
