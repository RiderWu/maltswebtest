package com.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class KeepExploring {
public WebDriver driver = null;
	
	public KeepExploring(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "KeepExploring_footer")
	private WebElement keepExploring;
	public WebElement getKeepExploring(){
		return keepExploring;
	}
	
	@FindBy(xpath = "//section[@id='KeepExploring_footer']/div/div/div[3]/div/div/a")
	private WebElement discoverOurCollection;
	public WebElement getDiscoverOurCollection(){
		return discoverOurCollection;
	}
	
	@FindBy(xpath = "//section[@id='KeepExploring_footer']/div/div/div[3]/div[2]/div/a")
	private WebElement discoverNewWhiskies;
	public WebElement getDiscoverNewWhiskies(){
		return discoverNewWhiskies;
	}
	
	@FindBy(xpath = "//section[@id='KeepExploring_footer']/div/div/div[3]/div[3]/div/a")
	private WebElement discoverSingleMalts;
	public WebElement getDiscoverSingleMalts(){
		return discoverSingleMalts;
	}
	
	@FindBy(className = "keep-exploring-bottom")
	private WebElement becomeAFriendOfTheClassicMalts;
	public WebElement getBecomeAFriendOfTheClassicMalts(){
		return becomeAFriendOfTheClassicMalts;
	}
	
	public WebElement getRegister() {
		try {
			return driver.findElement(By.cssSelector("div.keep-exploring-bottom > a.button.button--purple"));
		} catch (NoSuchElementException e) {
			return null;
		}
	}
}
