package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class Footer {
public WebDriver driver = null;
	
	public Footer(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "main-footer")
	private WebElement mainFooter;
	public WebElement getMainFooter(){
		return mainFooter;
	}
	
	@FindBy(xpath = "//td/div/ul")
	private WebElement top5ClassicsDesktop;
	public List<WebElement> getTop5ClassicsDesktop(){
		return top5ClassicsDesktop.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div/div[2]/div/div/ul")
	private WebElement top5ClassicsMobile;
	public List<WebElement> getTop5ClassicsMobile(){
		return top5ClassicsMobile.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//td[2]/div/ul")
	private WebElement atoDDesktop;
	public List<WebElement> getAtoDDesktop(){
		return atoDDesktop.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div/div[2]/div[2]/div/ul")
	private WebElement atoDMobile;
	public List<WebElement> getAtoDMobile(){
		return atoDMobile.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//td[3]/div/ul")
	private WebElement etoHDesktop;
	public List<WebElement> getEtoHDesktop(){
		return etoHDesktop.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div/div[2]/div[3]/div/ul")
	private WebElement etoHMobile;
	public List<WebElement> getEtoHMobile(){
		return etoHMobile.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//td[4]/div/ul")
	private WebElement itoNDesktop;
	public List<WebElement> getItoNDesktop(){
		return itoNDesktop.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div/div[2]/div[4]/div/ul")
	private WebElement itoNMobile;
	public List<WebElement> getItoNMobile(){
		return itoNMobile.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//td[5]/div/ul")
	private WebElement otoZDesktop;
	public List<WebElement> getOtoZDesktop(){
		return otoZDesktop.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div/div[2]/div[5]/div/ul")
	private WebElement otoZMobile;
	public List<WebElement> getOtoZMobile(){
		return otoZMobile.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//td[6]/ul")
	private WebElement guideToWhiskyDesktop;
	public List<WebElement> getGuideToWhiskyDesktop(){
		return guideToWhiskyDesktop.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div/div[2]/div/div[2]/ul")
	private WebElement guideToWhiskyMobile;
	public List<WebElement> getGuideToWhiskyMobile(){
		return guideToWhiskyMobile.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//td[7]/ul")
	private WebElement friendsOfTheClassicMaltsDesktop;
	public List<WebElement> getFriendsOfTheClassicMaltsDesktop(){
		return friendsOfTheClassicMaltsDesktop.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//div[3]/div/div[2]/ul")
	private WebElement friendsOfTheClassicMaltsMobile;
	public List<WebElement> getFriendsOfTheClassicMaltsMobile(){
		return friendsOfTheClassicMaltsMobile.findElements(By.tagName("a"));
	}
	
	@FindBy(xpath = "//td[8]/div/a")
	private List<WebElement> followUsLinksDesktop;
	public List<WebElement> getFollowUsLinksDesktop(){
		return followUsLinksDesktop;
	}
	
	@FindBy(xpath = "//div[4]/div/div[2]/a")
	private List<WebElement> followUsLinksMobile;
	public List<WebElement> getFollowUsLinksMobile(){
		return followUsLinksMobile;
	}
	
	@FindBys({
		@FindBy(id = "footer"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> bottomLinks;
	public List<WebElement> getBottomLinks(){
		return bottomLinks;
	}
	
	@FindBy(xpath = "//footer/div[2]/div/div/div")
	private WebElement moreMobile;
	public WebElement getMoreMobile(){
		return moreMobile;
	}
	
	@FindBy(css = "div.back-to-top.hidden-large > a")
	private WebElement backToTopMobile;
	public WebElement getBackToTopMobile(){
		return backToTopMobile;
	}

}
