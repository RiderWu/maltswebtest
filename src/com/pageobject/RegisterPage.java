package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class RegisterPage {
public WebDriver driver = null;
	
	public RegisterPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "registrationTitle")
	private WebElement registrationTitle;
	public Select getRegistrationTitle(){
		Select dropdownlist = new Select(registrationTitle);
		return dropdownlist;
	}
	
	@FindBy(id = "registrationFirstName")
	private WebElement registrationFirstName;
	public WebElement getRegistrationFirstName(){
		return registrationFirstName;
	}
	
	@FindBy(id = "registrationLastName")
	private WebElement registrationLastName;
	public WebElement getRegistrationLastName(){
		return registrationLastName;
	}
	
	@FindBy(id = "registrationDay")
	private WebElement registrationDay;
	public Select getRegistrationDay(){
		Select dropdownlist = new Select(registrationDay);
		return dropdownlist;
	}
	
	@FindBy(id = "registrationMonth")
	private WebElement registrationMonth;
	public Select getRegistrationMonth(){
		Select dropdownlist = new Select(registrationMonth);
		return dropdownlist;
	}
	
	@FindBy(id = "registrationYear")
	private WebElement registrationYear;
	public Select getRegistrationYear(){
		Select dropdownlist = new Select(registrationYear);
		return dropdownlist;
	}
	
	@FindBy(id = "registrationCountry")
	private WebElement registrationCountry;
	public Select getRegistrationCountry(){
		Select dropdownlist = new Select(registrationCountry);
		return dropdownlist;
	}
	
	@FindBy(id = "registrationEmail")
	private WebElement registrationEmail;
	public WebElement getRegistrationEmail(){
		return registrationEmail;
	}
	
	@FindBy(id = "registrationPassword")
	private WebElement registrationPassword;
	public WebElement getRegistrationPassword(){
		return registrationPassword;
	}
	
	@FindBy(id = "registrationPasswordConfirm")
	private WebElement registrationPasswordConfirm;
	public WebElement getRegistrationPasswordConfirm(){
		return registrationPasswordConfirm;
	}
	
	@FindBy(xpath = "//*[@id='registrationForm']/div[18]/div/div/label/div")
	private WebElement newsletterCheckBox;
	public WebElement getNewsletterCheckBox(){
		return newsletterCheckBox;
	}
	
	@FindBy(xpath = "//*[@id='registrationForm']/footer/div[1]/label/div[2]/div")
	private WebElement agreementCheckBox;
	public WebElement getAgreementCheckBox(){
		return agreementCheckBox;
	}
	
	@FindBy(css = "a.button.button--purple.button--submit")
	private WebElement registerButton;
	public WebElement getRegisterButton(){
		return registerButton;
	}
}
