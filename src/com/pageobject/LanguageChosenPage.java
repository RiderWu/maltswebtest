package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class LanguageChosenPage {
public WebDriver driver = null;
	
	public LanguageChosenPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBys({
		@FindBy(className = "main-container"),
		@FindBy(tagName = "a"),
	})
	private List<WebElement> chooseLanguages;
	public List<WebElement> getChooseLanguages(){
		return chooseLanguages;
	}
	
}
